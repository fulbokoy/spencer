package cl.fulbokoy.spencer.transactions.controller;

import cl.fulbokoy.spencer.accounts.entity.Account;
import cl.fulbokoy.spencer.accounts.entity.AccountType;
import cl.fulbokoy.spencer.transactions.entity.Expense;
import cl.fulbokoy.spencer.transactions.entity.Income;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureRestDocs
@AutoConfigureMockMvc
public class TransactionTest {

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    private ObjectMapper objectMapper;

    @Before
    public void setup(){
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context)
                .build();
        objectMapper = new ObjectMapper();
    }

    @Test
    public void addExpense() throws Exception {
        Account account = createTestAccount(1000L);

        Expense expense = new Expense();
        expense.setTransactionDate(new Date());
        expense.setReportedAmount(100L);
        expense.setAccount(account);

        mvc.perform(
                post("/api/expenses")
                        .content(objectMapper.writeValueAsString(expense))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mvc.perform(
                get("/api/accounts/" + account.getAccountId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.balance").value(900));

    }

    @Test
    public void updateExpenseAccount() throws Exception {
        Account account = createTestAccount(1000L);
        Account accountUpdate = createTestAccount(5000L);

        Expense expense = new Expense();
        expense.setTransactionDate(new Date());
        expense.setReportedAmount(100L);
        expense.setAccount(account);

        ResultActions resultActions = mvc.perform(
                post("/api/expenses")
                        .content(objectMapper.writeValueAsString(expense))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        Expense expenseUpdate = objectMapper.readValue(resultActions.andReturn().getResponse().getContentAsString(), Expense.class);
        expenseUpdate.setAccount(accountUpdate);

        mvc.perform(
                put("/api/expenses")
                        .content(objectMapper.writeValueAsString(expenseUpdate))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(
                get("/api/accounts/" + account.getAccountId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.balance").value(1000));

    }

    @Test
    public void addIncome() throws Exception {
        Account account = createTestAccount(1000L);

        Income income = new Income();
        income.setTransactionDate(new Date());
        income.setReportedAmount(100L);
        income.setAccount(account);

        mvc.perform(
                post("/api/incomes")
                        .content(objectMapper.writeValueAsString(income))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mvc.perform(
                get("/api/accounts/" + account.getAccountId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.balance").value(1100));

    }

    @Test
    public void updateIncome() throws Exception {
        Account account = createTestAccount(1000L);
        Account accountUpdate = createTestAccount(5000L);

        Income income = new Income();
        income.setTransactionDate(new Date());
        income.setReportedAmount(100L);
        income.setAccount(account);

        ResultActions resultActions = mvc.perform(
                post("/api/incomes")
                        .content(objectMapper.writeValueAsString(income))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        Income incomeUpdate = objectMapper.readValue(resultActions.andReturn().getResponse().getContentAsString(), Income.class);
        incomeUpdate.setAccount(accountUpdate);

        mvc.perform(
                put("/api/incomes")
                        .content(objectMapper.writeValueAsString(incomeUpdate))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(
                get("/api/accounts/" + account.getAccountId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.balance").value(1000));
    }


    public Account createTestAccount(Long balance) throws Exception {
        Account account = new Account("test account", AccountType.CREDIT, balance);
        ResultActions resultActions = mvc.perform(
                post("/api/accounts")
                        .content(objectMapper.writeValueAsString(account))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        return objectMapper.readValue(resultActions.andReturn().getResponse().getContentAsString(), Account.class);
    }
}
