package cl.fulbokoy.spencer.accounts.repository;

import cl.fulbokoy.spencer.accounts.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface AccountRepository  extends JpaRepository<Account, Integer> {

}
