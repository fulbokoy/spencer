package cl.fulbokoy.spencer.accounts.boundary;

import cl.fulbokoy.spencer.accounts.entity.Account;
import cl.fulbokoy.spencer.accounts.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/accounts")
@CrossOrigin("*")
public class AccountBoundary {

    @Autowired
    private AccountRepository accountRepository;

    @PostMapping
    @ResponseBody
    public ResponseEntity<Account> createAccount (@RequestBody Account account){
        Account createdAccount = accountRepository.save(account);

        return ResponseEntity.status(HttpStatus.CREATED).body(createdAccount);
    }

    @PutMapping
    @ResponseBody
    public ResponseEntity<Account> updateAccount (@RequestBody Account account){
        Account accountToUpdate = accountRepository.findOne(account.getAccountId());
        accountToUpdate.setBalance(account.getBalance());
        accountToUpdate.setName(account.getName());
        accountToUpdate.setType(account.getType());

        accountToUpdate = accountRepository.save(accountToUpdate);

        return ResponseEntity.status(HttpStatus.OK).body(accountToUpdate);
    }

    @DeleteMapping(value = "/{accountId}")
    @ResponseBody
    public void deleteAccount(@PathVariable Integer accountId){
        accountRepository.delete(accountId);
    }

    @GetMapping(value = "/{accountId}")
    @ResponseBody
    public Account getAccount(@PathVariable Integer accountId){
        return accountRepository.findOne(accountId);
    }

    @GetMapping
    @ResponseBody
    public List<Account> getAllAccounts(){
        return accountRepository.findAll();
    }
}
