package cl.fulbokoy.spencer.accounts.entity;

public enum AccountType {
    DEBIT,
    CREDIT,
    CASH
}
