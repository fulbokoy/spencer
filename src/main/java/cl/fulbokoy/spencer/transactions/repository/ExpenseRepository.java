package cl.fulbokoy.spencer.transactions.repository;

import cl.fulbokoy.spencer.transactions.entity.Expense;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface ExpenseRepository extends TransactionRepository<Expense>{
}
