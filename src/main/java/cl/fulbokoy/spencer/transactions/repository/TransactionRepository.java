package cl.fulbokoy.spencer.transactions.repository;

import cl.fulbokoy.spencer.transactions.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface TransactionRepository<T extends Transaction> extends JpaRepository<T, Integer> {
}
