package cl.fulbokoy.spencer.transactions.entity;

public enum IncomeType {
    DEBT,
    SALARY
}
