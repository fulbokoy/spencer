package cl.fulbokoy.spencer.transactions.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Expense extends Transaction {

    @Column
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
