package cl.fulbokoy.spencer.transactions.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class Income extends Transaction {

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private IncomeType type;

    public IncomeType getType() {
        return type;
    }

    public void setType(IncomeType type) {
        this.type = type;
    }
}
