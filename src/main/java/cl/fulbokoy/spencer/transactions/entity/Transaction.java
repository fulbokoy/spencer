package cl.fulbokoy.spencer.transactions.entity;

import cl.fulbokoy.spencer.accounts.entity.Account;
import cl.fulbokoy.spencer.budget.entity.Budget;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.internal.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "transaction")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private Integer transactionId;

    @Column(name = "transaction_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date transactionDate;

    @Column(name = "reported_amount")
    private Long reportedAmount;

    @OneToOne
    private Account account;

    @OneToOne
    @Nullable
    private Budget budget;

    public Transaction() {

    }

    public Transaction(Date transactionDate, Long reportedAmount) {
        this.transactionDate = transactionDate;
        this.reportedAmount = reportedAmount;
    }

    public Transaction(Date transactionDate, Long reportedAmount, Account account, Budget budget) {
        this(transactionDate, reportedAmount);
        this.account = account;
        this.budget = budget;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Long getReportedAmount() {
        return reportedAmount;
    }

    public void setReportedAmount(Long reportedAmount) {
        this.reportedAmount = reportedAmount;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }
}
