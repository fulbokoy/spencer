package cl.fulbokoy.spencer.transactions.boundary;

import cl.fulbokoy.spencer.transactions.controller.IncomeController;
import cl.fulbokoy.spencer.transactions.entity.Income;
import cl.fulbokoy.spencer.transactions.repository.IncomeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/incomes")
@CrossOrigin("*")
public class IncomeBoundary {

    @Autowired
    private IncomeRepository incomeRepository;

    @Autowired
    private IncomeController incomesController;

    @PostMapping
    @ResponseBody
    public ResponseEntity<Income> addIncome(@RequestBody Income income){
        Income createdIncome = incomesController.addIncome(income);

        return ResponseEntity.status(HttpStatus.CREATED).body(createdIncome);
    }

    @PutMapping
    @ResponseBody
    public ResponseEntity<Income> updateIncome(@RequestBody Income income){
        Income updatedIncome = incomesController.updateIncome(income);

        return ResponseEntity.ok(updatedIncome);
    }

    @DeleteMapping(value = "/{incomeId}")
    public void deleteIncome(@PathVariable Integer incomeId){
        incomeRepository.delete(incomeId);
    }

    @GetMapping(value = "/{incomeId}")
    @ResponseBody
    public void getIncome(@PathVariable Integer incomeId){
        incomeRepository.findOne(incomeId);
    }

    @GetMapping
    @ResponseBody
    public List<Income> getAllIncomes(){
        return incomeRepository.findAll();
    }
}
