package cl.fulbokoy.spencer.transactions.boundary;

import cl.fulbokoy.spencer.transactions.controller.ExpensesController;
import cl.fulbokoy.spencer.transactions.entity.Expense;
import cl.fulbokoy.spencer.transactions.repository.ExpenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/expenses")
@CrossOrigin("*")
public class ExpenseBoundary {

    @Autowired
    private ExpenseRepository expenseRepository;

    @Autowired
    private ExpensesController expensesController;

    @PostMapping
    @ResponseBody
    public ResponseEntity<Expense> addExpense(@RequestBody Expense expense){
        Expense createdExpense = expensesController.addExpense(expense);

        return ResponseEntity.status(HttpStatus.CREATED).body(createdExpense);
    }

    @PutMapping
    @ResponseBody
    public ResponseEntity<Expense> updateExpense(@RequestBody Expense expense){
        Expense updatedExpense = expensesController.updateExpense(expense);
        return ResponseEntity.ok(updatedExpense);
    }

    @DeleteMapping(value = "/{expenseId}")
    public void deleteExpense(@PathVariable Integer expenseId){
        expenseRepository.delete(expenseId);
    }

    @GetMapping(value = "/{expenseId}")
    @ResponseBody
    public void getExpense(@PathVariable Integer expenseId){
        expenseRepository.findOne(expenseId);
    }

    @GetMapping
    @ResponseBody
    public List<Expense> getAllExpenses(){
        return expenseRepository.findAll();
    }
}
