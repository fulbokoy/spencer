package cl.fulbokoy.spencer.transactions.controller;

import cl.fulbokoy.spencer.accounts.entity.Account;
import cl.fulbokoy.spencer.transactions.entity.Expense;
import cl.fulbokoy.spencer.transactions.repository.ExpenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExpensesController {

    private ExpenseRepository expenseRepository;

    private TransactionController transactionController;

    public Expense addExpense(Expense expense){
        Account account = expense.getAccount();
        transactionController.adjustAccountBalance(account, -expense.getReportedAmount());

        return expenseRepository.save(expense);
    }

    public Expense updateExpense(Expense expense){
        Expense previousExpense = expenseRepository.findOne(expense.getTransactionId());

        transactionController.adjustAccountBalance(previousExpense.getAccount(), previousExpense.getReportedAmount());
        Account updatedAccount = transactionController.adjustAccountBalance(expense.getAccount(), -expense.getReportedAmount());

        previousExpense.setAccount(updatedAccount);
        previousExpense.setBudget(expense.getBudget());
        previousExpense.setReportedAmount(expense.getReportedAmount());
        previousExpense.setTransactionDate(expense.getTransactionDate());

        return expenseRepository.save(previousExpense);
    }

    @Autowired
    public void setExpenseRepository(ExpenseRepository expenseRepository) {
        this.expenseRepository = expenseRepository;
    }

    @Autowired
    public void setTransactionController(TransactionController transactionController) {
        this.transactionController = transactionController;
    }
}
