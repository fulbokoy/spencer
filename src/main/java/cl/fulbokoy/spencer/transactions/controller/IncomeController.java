package cl.fulbokoy.spencer.transactions.controller;

import cl.fulbokoy.spencer.accounts.entity.Account;
import cl.fulbokoy.spencer.transactions.entity.Income;
import cl.fulbokoy.spencer.transactions.repository.IncomeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IncomeController {

    @Autowired
    private IncomeRepository incomeRepository;

    @Autowired
    private TransactionController transactionController;

    public Income addIncome(Income income){
        Account account = income.getAccount();
        transactionController.adjustAccountBalance(account, income.getReportedAmount());

        return incomeRepository.save(income);
    }

    public Income updateIncome(Income income){
        Income previousIncome = incomeRepository.findOne(income.getTransactionId());

        transactionController.adjustAccountBalance(previousIncome.getAccount(), -previousIncome.getReportedAmount());
        Account updatedAccount = transactionController.adjustAccountBalance(income.getAccount(), income.getReportedAmount());

        previousIncome.setAccount(updatedAccount);
        previousIncome.setBudget(income.getBudget());
        previousIncome.setReportedAmount(income.getReportedAmount());
        previousIncome.setTransactionDate(income.getTransactionDate());

        return incomeRepository.save(previousIncome);
    }

}
