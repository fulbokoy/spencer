package cl.fulbokoy.spencer.transactions.controller;

import cl.fulbokoy.spencer.accounts.entity.Account;
import cl.fulbokoy.spencer.accounts.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransactionController {

    private AccountRepository accountRepository;

    public Account adjustAccountBalance(Account account, Long value) {
        account.setBalance(account.getBalance() + value);

        return accountRepository.save(account);
    }

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }
}
