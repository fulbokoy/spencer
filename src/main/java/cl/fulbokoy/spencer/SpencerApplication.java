package cl.fulbokoy.spencer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpencerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpencerApplication.class, args);
    }
}
