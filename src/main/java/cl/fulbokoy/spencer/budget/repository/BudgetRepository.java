package cl.fulbokoy.spencer.budget.repository;

import cl.fulbokoy.spencer.budget.entity.Budget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface BudgetRepository extends JpaRepository<Budget, Integer> {
}
