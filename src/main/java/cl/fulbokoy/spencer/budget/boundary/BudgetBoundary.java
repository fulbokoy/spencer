package cl.fulbokoy.spencer.budget.boundary;

import cl.fulbokoy.spencer.budget.entity.Budget;
import cl.fulbokoy.spencer.budget.repository.BudgetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/budgets")
public class BudgetBoundary {

    @Autowired
    private BudgetRepository budgetRepository;

    @PostMapping
    @ResponseBody
    public ResponseEntity<Budget> createBudget(@RequestBody Budget budget){
        Budget createdBudget = budgetRepository.save(budget);

        return ResponseEntity.status(HttpStatus.CREATED).body(createdBudget);
    }

    @PutMapping
    @ResponseBody
    public ResponseEntity<Budget> updateBudget(@RequestBody Budget budget){
        Budget budgetToUpdate = budgetRepository.findOne(budget.getBudgetId());
        budgetToUpdate.setName(budget.getName());
        budgetToUpdate.setAmount(budget.getAmount());
        budgetToUpdate.setFromDate(budget.getFromDate());
        budgetToUpdate.setToDate(budget.getToDate());

        budgetToUpdate = budgetRepository.save(budgetToUpdate);

        return ResponseEntity.ok(budgetToUpdate);
    }

    @DeleteMapping(value = "/{budgetId}")
    public void deleteBudget(@PathVariable Integer budgetId){
        budgetRepository.delete(budgetId);
    }

    @GetMapping(value = "/{budgetId}")
    @ResponseBody
    public void getBudget(@PathVariable Integer budgetId){
        budgetRepository.findOne(budgetId);
    }

    @GetMapping
    @ResponseBody
    public List<Budget> getAllBudgets(){
        return budgetRepository.findAll();
    }
}
