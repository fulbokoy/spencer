INSERT INTO `transaction` (transaction_id, transaction_date, reported_amount, account_id, budget_id) VALUES (1, CURRENT_DATE, 1000, 1, null);
INSERT INTO `expense` (transaction_id, description) VALUES (1, 'some wasted money');
INSERT INTO `transaction` (transaction_id, transaction_date, reported_amount, account_id, budget_id) VALUES (2, CURRENT_DATE, 4500, 1, null);
INSERT INTO `expense` (transaction_id, description) VALUES (2, 'more wasted money');

INSERT INTO `transaction` (transaction_id, transaction_date, reported_amount, account_id, budget_id) VALUES (3, CURRENT_DATE, 8500, 1, null);
INSERT INTO `income` (transaction_id, `type`) VALUES (3, 'DEBT');